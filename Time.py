from flask import Flask, jsonify
from datetime import datetime


app = Flask(__name__)


@app.route('/')
def json():
    json = {
        'time': datetime.now(),
        'text': 'Hello, world'
    }

    return jsonify(json)


if __name__ == '__main__':
    app.run(host='0.0.0.0')

